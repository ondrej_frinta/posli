///////////////
// DOM READY //
///////////////

$(function () {
	var $container = $('#container'),
		$header = $('#header'),
		$menu = $('#jsNav'),
		$menuItems = $menu.find('li>a'),
		spyData = [],
		spyDataLen = 0,
		headerH = $header.height(),
		windowH = $(window).height(),
		fixOffset = windowH - headerH + 5,
		maxScroll = $(document).height() - windowH;


	// scroll spy
	$menuItems.each(function () {
		var href = $(this).attr('href');
		spyData.push({
			'a': $(this),
			'li': $(this).parent('li'),
			'href': href,
			'offset': $(href).offset().top
		});
	});
	spyDataLen = spyData.length;


	// Window resize handler
	$(window).on('resize', function () {
		windowH = $(window).height();
		fixOffset = windowH - headerH;
		maxScroll = $(document).height() - windowH;
	});

	// Fix menu if scrolled out
	$(window).on('scroll', function (e) {
		windowScrollHandler($(window).scrollTop());
	});
	windowScrollHandler($(window).scrollTop());
	// prevents menu flash if window is scrolled
	setTimeout(function() {$header.addClass('animate');}, 300);

	// Smooth slide to target
	$('a[href^="#"]').on('click', function (e) {
		e.preventDefault();
		var $this = $(this),
			target = this.hash,
			$target = $(target);

		if ($target.size() == 0) return;

		//hide hash on scroll top
		if (target == "#home") {
			target = '';
		}

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top + 1
		}, 650, function () {
			window.location.hash = target;
		});
	});


	// Setup forms
	$(".requestForm input")
		.on('input', function () {
			// show/hide input label
			var $input = $(this);

			if(!$input.val()) {
				$input.siblings('label').fadeIn(150);
			} else {
				$(this).siblings('label').fadeOut(200);
			}

			// remove invalid class
			$input.removeClass('invalid');
		})
		.on('focusout', function () {
			// Add validation class to form input
			var $input = $(this);

			if ($input.is(":invalid")) {
				$input.addClass('invalid');
			} else {
				$input.removeClass('invalid');
			}
		})
		.each(function () {
			if ($(this).val() != "") {
				$(this).siblings('label').hide();
			}
		});

	$("form.requestForm").on('submit', function(event) {
		event.preventDefault();

		var $form = $(this),
			$requestFormSent = $('<div class="requestFormSent"></div>');

		$requestFormSent.load( "inc/request-form-sent.html", function() {
			$requestFormSent.hide();
			$form.fadeOut(200, function() {
				$form.after($requestFormSent);
				$requestFormSent.fadeIn(250);
			});
		});


	});


	/**
	 * Window scroll handler
	 * @param Int offset
	 **/
	function windowScrollHandler(offset) {
		var scrollOffset = offset;

		// vertical
		if (scrollOffset > fixOffset) {
			$header.addClass('fixed');
		} else if ($header.hasClass('fixed')) {
			$header.removeClass('fixed');
		}

		//scrollspy
		$menuItems.removeClass('active');
		if (scrollOffset >= maxScroll) {
			spyData[spyDataLen - 1].a.addClass('active');
		} else {
			for (var i = 0; i < spyDataLen - 1; i++) {
				if (scrollOffset >= spyData[i].offset && scrollOffset < spyData[i + 1].offset) {
					spyData[i].a.addClass('active');
					break;
				}
			}
		}

	};

});
